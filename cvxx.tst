// This file is released into the public domain
//=================================
// load fact
if ~isdef('cvxx')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
    // chargement des données 
    load('x_pca_pls.dat');
    x=x_pca_pls.x;
    y=x_pca_pls.y;
    
    // adaptation à x_sameobs et prefixed_blocks
    x=x(1:40,:);
    y=y(1:40,:);
    
    
    // Données: 20 échantillons avec des répétitions -> 40 mesures
    x_sameobs=[1 1 3 3 2 2 4 5 4 3 6 7 6 5 8 9 10 11 12 12 11 10 6 13 14 15 16 17 18 19 20 20 20 19 19 18 17 5 11 10]; 
    prefixed_blocks=[1 1 1 2 2 2 3 3 3 3 3 3 2 2 2 1 1 1 1 2 ; 1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3; 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 3 ]';


    // paramètres de la version d’avant Nov.18
    [secv11,predcv11,dof11] = cvxx(x,y,5,'pls',1,10);
    [secv12,predcv12,dof12] = cvxx(x,y,'blk5','pls',1,10);
    [secv13,predcv13,dof13] = cvxx(x,y,'jck5','pls',1,10);
    [secv14,predcv14,dof14] = cvxx(x,y,[5 4],'pls',1,10);
    

    // paramètres de la version d’après Nov.18
    split1=struct();
    split1.method='random'; 
    split1.nblocks=3;
    split1.nbrepeat=10;
    split1.sameobs=x_sameobs;

    split2=struct();
    split2.method='jack';
    split2.nblocks=3;
    split2.nbrepeat=1;       // inutile de répéter avec ‘jack’ ou ‘venitian’
    split2.sameobs=x_sameobs;

    split3=struct();
    split3.method='venitian';
    split3.nblocks=3;
    split3.nbrepeat=1;      // inutile de répéter avec ‘jack’ ou ‘venitian’
    split3.sameobs=x_sameobs;

    split4=struct();
    split4.method='prefixed';
    split4.nblocks=prefixed_blocks;
    split4.sameobs=x_sameobs;

    [secv21,predcv21,dof21,lot21] = cvxx(x,y,split1,'pls',1,10);
    [secv22,predcv22,dof22,lot22] = cvxx(x,y,split2,'pls',1,10);
    [secv23,predcv23,dof23,lot23] = cvxx(x,y,split3,'pls',1,10);
    [secv24,predcv24,dof24,lot24] = cvxx(x,y,split4,'pls',1,10);
    
    // essais avec en entrée les matrices disjonctives
    
    split1b=split1;
    split1b.method='prefixed';
    split1b.nblocks=lot21;
    
    split2b=split2;
    split2b.method='prefixed';
    split2b.nblocks=lot22;
    
    split3b=split3;
    split3b.method='prefixed';
    split3b.nblocks=lot23;
    
    split4b=split4;
    split4b.method='prefixed';
    split4b.nblocks=lot24;
    
    [secv21b,predcv21b,dof21b,lot21b] = cvxx(x,y,split1b,'pls',1,10);
    [secv22b,predcv22b,dof22b,lot22b] = cvxx(x,y,split2b,'pls',1,10);
    [secv23b,predcv23b,dof23b,lot23b] = cvxx(x,y,split3b,'pls',1,10);
    [secv24b,predcv24b,dof24b,lot24b] = cvxx(x,y,split4b,'pls',1,10);
    
    if (sum(abs(secv21b-secv21))>100*%eps) | (sum(abs(secv22b-secv22))>100*%eps) | (sum(abs(secv23b-secv23))>100*%eps) | (sum(abs(secv24b-secv24))>100*%eps)  then
        pause
    end
    

  clear x_sameobs prefixed_blocks  
  clear split1 split2 split3 split4 split1b split2b split3b split4b  
  clear secv11 secv12 secv13 secv14 secv21 secv22 secv23 secv24 secv21b secv22b secv23b secv24b
  clear predcv11 predcv12 predcv13 predcv14 predcv21 predcv22 predcv23 predcv24 predcv21b predcv22b predcv23b predcv24b 
  clear dof11 dof12 dof13 dof14 dof21 dof22 dof23 dof24 dof21b dof22b dof23b dof24b  
  clear lot21 lot22 lot23 lot24 lot21b lot22b lot23b lot24b
  clear x_pca_pls x y ans
//=======================================================================================
