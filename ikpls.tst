// This file is released into the public domain
//=================================
// load saisir
if ~isdef('ikpls')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  load('x_pca_pls.dat')  // Cargill dataset
  
  x=x_pca_pls.x;
  y=x_pca_pls.y;         // third variable
  b20_raw=x_pca_pls.b20_pls_brut;
  b20_centred=x_pca_pls.b20_pls_centre;
  
  res=ikpls(x,y,4,20,0);  // raw
  r=correl(res.b.d(:,20),b20_raw);
  r2=r^2;
  if ~isdef('r2') | r2<0.99 then pause;
  end
 
  res=ikpls(x,y,4,20,1);   // centred
  r=correl(res.b.d(:,20),b20_centred);
  r2=r^2;
  if ~isdef('r2') | r2<0.99 then pause;
  end
 
  
  clear x y b20_raw b20_centred res r r2 x_pca_pls
//================================================
