// This file is released into the public domain
//=================================
if ~isdef('glx_mat2div')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================resume
 
 
  // -------------------------------------------------------
  // ATTENTION: CRASH AVEC SCILAB 6.0 (BUG AVEC savematfile)
  // -------------------------------------------------------
 
  sci_version=strtod(part(getversion(),[8 ;9 ;10]));
  
  // début d'une instruction if
  if sci_version < 6 then


    // -----------------------
    // exemple 1: régressions
   
    // obtention des données:
    load('x_pca_pls.dat')  // Cargill dataset
    x1=div(x_pca_pls.x);
    y1=div(x_pca_pls.y); 
    res=pls(x1,y1,5,20);
  
    res2=regapply(res,x1,y1);

    // application de div2mat: 
    matname='temp_respls.mat';
    res3=div2mat(res,matname);
  
    // application de mat2div:
    res3bis=glx_mat2div(matname);
  
    res3ter=regapply(res3bis,x1,y1);
  
    // validation de l'existance de res3bis 
    if  ~isdef('res3bis') then
        pause
    end
  
    // validation de la non-perte de données avec div2mat et glx_mat2div:
    if max(abs(res2.r2.d-res3ter.r2.d))> 1000* %eps then
        error('problem with mat2div or div2mat')
    end



    // ---------------------------------
    // exemple 2: PLSDA
  
    // obtention des données: 
    load x_140farines.dat

    x=xfarines;
    
 
    // modèle PLS-DA:
    [model_plsda]=plsda(x,groupes,4,4);
    [model2_plsda_bis]=daapply(model_plsda,x,groupes);
  
    // applicaton de div2mat et glx_mat2div
    matname2='temp_resplsda.mat';
    model3_plsda=div2mat(model_plsda,matname2);
    model3bis_plsda=glx_mat2div(matname2);
    model3ter_plsda=daapply(model3bis_plsda,x,groupes);
  
    // comparaison des résultats: direct / div2mat +glx_mat2div
    diff1=max(abs(model2_plsda_bis.err_test.d - model3ter_plsda.err_test.d));
    if diff1>1000*%eps then
       pause 
    end



    // ---------------------------------------------------------------
    // exemple 3: ACOM (choisi car les sorties comprennent des listes)

    // obtention des données
    loadmatfile x_acom1.mat
    x2=list();
    x2(1)=x.col1;
    x2(2)=x.col2;
    x2(3)=x.col3;
    x2(4)=x.col4;
    x2(5)=x.col5;
  
    res_acom=acom1(x2,8);

    matname3='temp_resacom.mat';
    res5=div2mat(res_acom,matname3);

    res5bis=glx_mat2div(matname3);
    
    // validation du résultat sur quelques données: 
    diff1= max(abs(res_acom.global_scores.d - res5bis.global_scores.d));
    diff2= max(abs(res_acom.individual_loadings(5).d - res5bis.individual_loadings(5).d)); 
   
    if diff1 > 1000*%eps | diff2 > 1000*%eps then
        pause
    end     


    // --------------------------------------------------------------
    // exemple 4: données EPO (choisies car les plus compliquées)

    load('x_res_epo.dat')
    
    res6=div2mat(res_epo, 'temp_x_res_epo.mat');
    
    res6bis=glx_mat2div('temp_x_res_epo.mat');
   
    // validation du résultat sur quelques données: 
    diff1=max(abs(res_epo.d_matrix.d - res6bis.d_matrix.d));
    diff2=max(abs(res_epo.pls_models(3).err.d - res6bis.pls_models(3).err.d));
     
    if diff1 > 1000*%eps | diff2 > 1000*%eps then
        pause
    end 

  // fin de l'instruction if du tout début (5.5 vs. 6.0)
  end 

 
  // nettoyage:
  
  deletefile('temp_respls.mat');
  deletefile('temp_resplsda.mat');
  deletefile('temp_resacom.mat');
  deletefile('temp_x_res_epo.mat');
  
  clear x x1 x2 y y1 res res2 res3 res3bis res3ter x_pca_pls res4 res4bis res_acom matname matname2 
  clear matname3 model3ter_plsda model3bis_plsda model2_plsda_bis model_plsda model groupes
  clear xfarines difference s1 s2 s3 s4 m1 m2 m3 m4 model2 groupes2 conf_cv conf_cal
  clear vp_cal err_cal_2 err_cv_2 conf_cv_2 conf_cal_2 vp_cal_2
  clear model3_plsda err_cal err_cv sci_version
  clear res5 res5bis res6 res6bis diff1 diff2 res_epo
 

  // ================================================================================
  
  
  
  
  
