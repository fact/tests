// This file is released into the public domain
//=================================
// load fact
if ~isdef('mz_hdf5tosci')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x=mz_hdf5tosci('x_180509_10.h5',10);
  
  if ~isdef('x') then
      pause
  end
  
  clear x
//=================================
