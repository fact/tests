// This file is released into the public domain
//=================================
// load fact
if ~isdef('covsel')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8];
  y=[3;4;2;5];
  
  [vs]=covsel(x,y,20);
  
  if ~isdef('vs') then pause;
  end
  
  clear vs x y
//=================================
