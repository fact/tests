// This file is released into the public domain
//=================================
// load fact

// <-- TEST WITH GRAPHIC -->

if ~isdef('regplot')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  y1=[1;2;3;4;5;6;7;8;9;10];
  y2=[1.1;1.9;3.05;4.2;4.95;5.9;7;8.01;9.2;9.9];
  
  regplot(y1,y2,'r*','','mg/L','ethanol');
  
  h=get_figure_handle(0);
  
  if ~isdef('h') | isempty(h)  then pause;
  end
  
  clear y1 y2 h;
  close();
//=================================
