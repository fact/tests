// This file is released into the public domain
//=================================
// load fact
if ~isdef('calnnpls')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  load('x_pca_pls.dat')
  x=x_pca_pls.x;
  y=x_pca_pls.y;
  
  //y=classe(:,1)+ 2* classe(:,2) ;

  ypred=calnnpls(x,y,x,2,2);
  
  if ~isdef('ypred') then pause;
  end
  
  clear ypred testx x y classe x_pca_pls
//======================================================================
