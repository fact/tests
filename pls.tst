// This file is released into the public domain
//=================================

  if ~isdef('pls')  then
     root_tlbx_path = SCI+'\contrib\fact\';   
     exec(root_tlbx_path + 'loader.sce',-1); 
  end
  //=================================
 
  load('x_pca_pls.dat')  // Cargill dataset
  
  x=x_pca_pls.x;
  y=x_pca_pls.y;         // third variable
  b20_raw=x_pca_pls.b20_pls_brut;
  b20_centred=x_pca_pls.b20_pls_centre;
  
  res=pls(x,y,4,20,0);  // raw
  r=correl(res.b.d(:,20),b20_raw);
  r2=r^2;
  if ~isdef('r2') | r2<0.99 then 
      pause;
  end
 
  res=pls(x,y,4,20,1);   // centred
  r=correl(res.b.d(:,20),b20_centred);
  r2=r^2;
  if ~isdef('r2') | r2<0.99 then 
      pause;
  end
 
  // test avec split=une structure
  x_sameobs=[[1:20]' ; [1:20]';[1:20]';[1:20]'];
  split.method='random';
  split.nblocs=2
  split.nbrepeat=10;  
  split.sameobs=x_sameobs;
  res=pls(x,y,split,20,1);
  if ~isdef('res') then
      pause
  end

  clear x y b20_raw b20_centred res r r2 x_pca_pls x_sameobs split
//=================================================================
