// This file is released into the public domain
//=================================
// load fact
if ~isdef('cvgroups')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
    
    // Données: 20 échantillons avec des répétitions -> 40 mesures
    x_sameobs=[1 1 3 3 2 2 4 5 4 3 6 7 6 5 8 9 10 11 12 12 11 10 6 13 14 15 16 17 18 19 20 20 20 19 19 18 17 5 11 10]; 
    prefixed_blocks=[1 1 1 2 2 2 3 3 3 3 3 3 2 2 2 1 1 1 1 2 ; 1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3; 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 3 ]';


    // paramètres de la version d’avant Nov.18
    lot11=cvgroups(20,'jck5');
    lot12=cvgroups(20,'blk5'); 
    lot13=cvgroups(20, 5);
    lot14=cvgroups(20,[5 4]);
    lot15=cvgroups(20,'vnb5');

    // paramètres de la version d’après Nov.18
    split1=struct();
    split1.method='random'; 
    split1.nblocks=3;
    split1.nbrepeat=10;
    split1.sameobs=x_sameobs;

    split2=struct();
    split2.method='jack';
    split2.nblocks=3;
    split2.nbrepeat=1; 		// inutile de répéter avec ‘jack’ ou ‘venitian’
    split2.sameobs=x_sameobs;

    split3=struct();
    split3.method='venitian';
    split3.nblocks=3;
    split3.nbrepeat=1;      // inutile de répéter avec ‘jack’ ou ‘venitian’
    split3.sameobs=x_sameobs;

    split4=struct();
    split4.method='prefixed';
    split4.nblocks=prefixed_blocks;
    split4.sameobs=x_sameobs;

    lot21=cvgroups(20,split1);
    lot22=cvgroups(20,split2);
    lot23=cvgroups(20,split3);
    lot24=cvgroups(20,split4);

  clear x_sameobs prefixed_blocks lot11 lot12 lot13 lot14 lot15 split1 split2 split3 split4 lot21 lot22 lot23 lot24 
//=================================================================================================================
